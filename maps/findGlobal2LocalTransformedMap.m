function [ transformedMap ] = findGlobal2LocalTransformedMap(minX,minY,maxX,maxY,resolution,translation,rotationAngle,bigMapResolution,bigMapMinX,bigMapMinY,bigMap)
%FINDTRANSFORMEDMAP Summary of this function goes here
%   just generate all the points in the transformed map and then map them
%   to the real Map
[X,Y] = meshgrid(minX:resolution:maxX,minY:resolution:maxY);
originalCoordinates = [X(:),Y(:)];
transformedMap = zeros(size(X'));
transformedCoordinates = transformPoint2Global(originalCoordinates,rotationAngle,translation);

globalCoordinates = coord2index(transformedCoordinates(:,1),transformedCoordinates(:,2),bigMapResolution,bigMapMinX,bigMapMinY);
localCoordinates = coord2index(originalCoordinates(:,1),originalCoordinates(:,2),resolution,minX,minY);
localInd = sub2ind(size(transformedMap), localCoordinates(:,1), localCoordinates(:,2));
globalInd = sub2ind(size(bigMap), globalCoordinates(:,1), globalCoordinates(:,2));

transformedMap(localInd(:)) = bigMap(globalInd(:));
end


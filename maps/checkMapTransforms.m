minMapX = 0;
minMapY = 0;
minMapTheta = 0;
resolution = 10;
maxMapX = 1100;
maxMapY = 1100;
maxMapTheta = 2*pi;
map = getMapfromImage('simpleMap.jpg');

%% checking global to local
minLocalX = 0;
minLocalY = -100;
maxLocalX = 200;
maxLocalY = 100;
translation = [770,1010];
rotationAngle = deg2rad(10);
localMapResolution = 10;

checkMapTransformGlobal2Local(minLocalX,minLocalY,maxLocalX,maxLocalY,localMapResolution,translation,rotationAngle,resolution,minMapX,minMapY,map);

%% checking global to local
close all
[X,Y] = meshgrid(minLocalX:resolution:maxLocalX,minLocalY:resolution:maxLocalY);
localMap = zeros(size(X'));
checkMapTransformLocal2Global(minLocalX,minLocalY,maxLocalX,maxLocalY,localMapResolution,localMap,translation,rotationAngle,resolution,minMapX,minMapY,map);
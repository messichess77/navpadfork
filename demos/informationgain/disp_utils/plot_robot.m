function [ h ] = plot_robot( state, triangle_params, map)
%PLOT_ROBOT Summary of this function goes here
%   Detailed explanation goes here
if nargin<2
    base = 3;
    height = 1;
    [ X,Y ] = form_triangle( base, height);
else
    [ X,Y ] = form_triangle( triangle_params.base, triangle_params.height);
end
p = [X,Y];
p = transformPoint2Global(p,state.psi,[state.x state.y]);
p = coord2indexNotRound(p(:,1),p(:,2),map.scale,map.min(1),map.min(2));
h = patch(p(:,1),p(:,2),'r');
set(h,'FaceAlpha',0.5);
end


function belief = expose_map_belief( bounding_box, map, belief )
%UNTITLED8 Summary of this function goes here
%   Detailed explanation goes here

map(map<0.01) = 0.01;
map(map>0.99) = 0.99;
log_odds = probfree2logodds( map );

bounding_box(:,1) = max( ones(size(bounding_box(:,1))), min( size(map, 1)*ones(size(bounding_box(:,1))) , round(bounding_box(:,1))));
bounding_box(:,2) = max( ones(size(bounding_box(:,2))), min( size(map, 2)*ones(size(bounding_box(:,2))) , round(bounding_box(:,2))));

belief(bounding_box(1,1):bounding_box(2,1),bounding_box(1,2):bounding_box(2,2)) = log_odds(bounding_box(1,1):bounding_box(2,1),bounding_box(1,2):bounding_box(2,2));
end

